//
//  Moneda+CoreDataProperties.swift
//  Test_bcp
//
//  Created by Donnadony Mollo on 17/02/22.
//
//

import Foundation
import CoreData


extension Moneda {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Moneda> {
        return NSFetchRequest<Moneda>(entityName: "Moneda")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var nombre: String?
    @NSManaged public var valor: NSDecimalNumber?

}

extension Moneda : Identifiable {

}
