//
//  Transacciones+CoreDataProperties.swift
//  Test_bcp
//
//  Created by Donnadony Mollo on 17/02/22.
//
//

import Foundation
import CoreData


extension Transacciones {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Transacciones> {
        return NSFetchRequest<Transacciones>(entityName: "Transacciones")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var fecha: Date?
    @NSManaged public var tipo: String?
    @NSManaged public var monto: Double

}

extension Transacciones : Identifiable {

}
