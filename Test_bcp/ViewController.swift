//
//  ViewController.swift
//  Test_bcp
//
//  Created by Donnadony Mollo on 17/02/22.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet weak var btnCambiar: UIButton!
    
    
    @IBOutlet weak var txtEnviar: UITextField!
    
    @IBOutlet weak var txtRecibir: UITextField!
    
    
    @IBOutlet weak var lblVenta: UILabel!
    
    @IBOutlet weak var lblCompra: UILabel!
    
    var isChangeDolar = true
    
    @IBOutlet weak var lblTextoEnviar: UILabel!
    
    @IBOutlet weak var lblTextoRecibir: UILabel!
    @IBOutlet weak var btnDolares: UIButton!
    
    @IBOutlet weak var btnSoles: UIButton!
    
    private var models = [Transacciones]()
    var montoDeposito = ""
    @IBAction func btnEnviarCambio(_ sender: Any) {
        
        var saved = true
        if txtEnviar.text == "" {
            saved = false
        }
        
        if txtRecibir.text == "" {
            saved = false
        }
        
        
        
        if saved {
            enviarAlerta(title: "BCP", message: "Desea enviar la transaccion para que sea procesada?", completion:  grabarTransaccion())
        }
        else{
            enviarAlerta(title: "BCP", message: "Ingrese datos.", completion:  ())
        }
        
        
        
    }
    
    func getTransaccions(){
        do{
            models = try context.fetch(Transacciones.fetchRequest())
            
            for transaccion in models {
                print("Transaccion : \(transaccion.id!) - tipo \(transaccion.tipo!) - monto : \(transaccion.monto)")
            }
        }
        catch{
            
        }
    }
    
    
    func createTransaccion(tipo: String, monto: Double){
        
        
        let newItem = Transacciones(context: context)
        newItem.id = UUID()
        newItem.fecha = Date()
        newItem.tipo = tipo
        newItem.monto = monto
        
        do {
            try context.save()
        }
        catch{
            
        }
        
    }
    
    
    
    func grabarTransaccion(){
        print("Se envio la transaccion")
        
        let tipo = isChangeDolar ? "ChangeDolar" : "ChangeSoles"
        
        createTransaccion(tipo: tipo, monto: montoDeposito.toDouble() ?? 0)
        
        DispatchQueue.main.async {
            self.getTransaccions()
        }
    }
    
    
    var venta = 3.35
    var compra = 3.01
    
    
    @IBAction func btnCambiar(_ sender: Any) {
        
        isChangeDolar = !isChangeDolar
        
        btnDolares.titleLabel?.text = isChangeDolar ? "Dólares" : "Soles"
        btnSoles.titleLabel?.text = isChangeDolar ? "Soles" : "Dólares"
        //lblTextoEnviar.text = isChangeDolar
        
        
        print("cambio dolar \(isChangeDolar)")
        calcularEnvio()
        
    }
    
    @IBAction func btnRecibirTouch(_ sender: Any) {
        
        print("Se clickeo en recibir")
        
    }
    
    @IBAction func txtRecibirChanged(_ sender: Any) {
        
        var cambio: Double = 0.00
        
        if isChangeDolar{
            let soles = txtRecibir.text!.toDouble() ?? 0
            cambio = soles / compra
            let resultado = String(format: "%.2f", cambio)
            if soles == 0{
                txtEnviar.text = "No valido"
            }
            else{
                txtEnviar.text = resultado
            }
            print("Enviar change\(resultado)")
            
            //txtRecibir.text = resultado
            
        }
        else{
            let soles = txtRecibir.text!.toDouble() ?? 0
            cambio = soles * venta
            let resultado = String(format: "%.2f", cambio)
            if soles == 0{
                txtEnviar.text = "No valido"
            }
            else{
                txtEnviar.text = resultado
            }
        }
        montoDeposito = String(format: "%.2f", cambio)
        
        
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func txtEnviarEditingChanged(_ sender: Any) {
        
        
        calcularEnvio()
    }
    
    func calcularEnvio(){
        
        var cambio: Double = 0.00
        if isChangeDolar{
            let dolares = txtEnviar.text!.toDouble() ?? 0
            cambio = dolares * compra
            let resultado = String(format: "%.2f", cambio)
            if dolares == 0{
                txtRecibir.text = "No valido"
            }
            else{
                txtRecibir.text = resultado
            }
            
            
            print("Enviar change\(resultado)")
            
            //txtRecibir.text = resultado
            
        }
        else{
            let dolares = txtEnviar.text!.toDouble() ?? 0
            cambio = dolares / venta
            let resultado = String(format: "%.2f", cambio)
            if dolares == 0{
                txtRecibir.text = "No valido"
            }
            else{
                txtRecibir.text = resultado
            }
        }
        
        montoDeposito = String(format: "%.2f", cambio)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //view.setBackgroundImage(<#T##UIImage?#>, for: <#T##UIControl.State#>)
        // Do any additional setup after loading the view.
        
        personalizeButtons()
        initView()
        
        
       // btnCambiar.click
    }
    
    func initView(){
        
        
        getTransaccions()
        //Looks for single or multiple taps.
             let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

            //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
            //tap.cancelsTouchesInView = false

            view.addGestureRecognizer(tap)
        
        lblVenta.text = "\(venta)"
        lblCompra.text = "\(compra)"
        txtEnviar.becomeFirstResponder()
        print("none")
    }
    
    
    func enviarAlerta(title: String, message: String, completion: ()){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            completion
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func personalizeButtons (){
        btnCambiar.layer.cornerRadius = btnCambiar.frame.width / 2
        btnCambiar.layer.masksToBounds = true
    }

    
  

}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension UIView {
    func borders(for edges:[UIRectEdge], width:CGFloat = 1, color: UIColor = .black) {

        if edges.contains(.all) {
            layer.borderWidth = width
            layer.borderColor = color.cgColor
        } else {
            let allSpecificBorders:[UIRectEdge] = [.top, .bottom, .left, .right]

            for edge in allSpecificBorders {
                if let v = viewWithTag(Int(edge.rawValue)) {
                    v.removeFromSuperview()
                }

                if edges.contains(edge) {
                    let v = UIView()
                    v.tag = Int(edge.rawValue)
                    v.backgroundColor = color
                    v.translatesAutoresizingMaskIntoConstraints = false
                    addSubview(v)

                    var horizontalVisualFormat = "H:"
                    var verticalVisualFormat = "V:"

                    switch edge {
                    case UIRectEdge.bottom:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "[v(\(width))]-(0)-|"
                    case UIRectEdge.top:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v(\(width))]"
                    case UIRectEdge.left:
                        horizontalVisualFormat += "|-(0)-[v(\(width))]"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    case UIRectEdge.right:
                        horizontalVisualFormat += "[v(\(width))]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    default:
                        break
                    }

                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: horizontalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: verticalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                }
            }
        }
    }
}

